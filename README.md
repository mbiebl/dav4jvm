
[![build status](https://gitlab.com/bitfireAT/dav4jvm/badges/master/build.svg)](https://gitlab.com/bitfireAT/dav4jvm/commits/master)


# dav4jvm

dav4jvm is a WebDAV/CalDAV/CardDAV library for JVM (Java/Kotlin). It was
developed for [DAVx⁵](https://www.davx5.com) initially.

Original repository: https://gitlab.com/bitfireAT/dav4jvm/

Generated KDoc: https://bitfireAT.gitlab.io/dav4jvm/dokka/dav4jvm/


## Contact / License

dav4jvm is licensed under [Mozilla Public License, v. 2.0](LICENSE).

For questions, suggestions etc. please use this forum:
https://forums.bitfire.at/category/18/libraries

If you want to contribute, please work in your own repository and then
notify us on your changes so that we can backport them.

Email: [play@bitfire.at](mailto:play@bitfire.at)


## Contributors

  * Ricki Hirner (initial contributor)
  * David González Verdugo (dgonzalez@owncloud.com)
  * Matt Jacobsen (https://gitlab.com/mattjacobsen)

